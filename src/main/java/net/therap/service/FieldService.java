package net.therap.service;

import net.therap.domain.Field;

import java.util.List;

/**
 * @author shadman
 * @since 12/24/17
 */
public interface FieldService {

    public void addField(Field field);

    public Field getFieldById(long fieldId);

    public List<Field> getAllFields();

    public void deleteField(Field field);
}
