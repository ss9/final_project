package net.therap.service;

import net.therap.dao.FieldDao;
import net.therap.domain.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author shadman
 * @since 12/24/17
 */

@Service("fieldService")
public class FieldServiceImpl implements FieldService {

    @Autowired
    private FieldDao fieldDao;

    @Override
    public void addField(Field field) {
        fieldDao.addField(field);
    }

    @Override
    public Field getFieldById(long fieldId) {
        return fieldDao.getFieldById(fieldId);
    }

    @Override
    public List<Field> getAllFields() {
        return fieldDao.getAllFields();
    }

    @Override
    public void deleteField(Field field) {
        fieldDao.deleteField(field);
    }
}
