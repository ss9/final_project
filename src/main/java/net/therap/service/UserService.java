package net.therap.service;

import net.therap.domain.User;

import java.util.List;

/**
 * @author shadman
 * @since 12/24/17
 */
public interface UserService {

    public User getUserById(long userId);

    public List<User> getAllUsers();

    public List<User> getUsersByUsername(String username);

    public void addUser(User user);

    public void updateUser(User oldUser, User newUser);

    public void deleteUser(User user);
}
