package net.therap.service;

import net.therap.dao.BookingDao;
import net.therap.domain.Booking;
import net.therap.domain.Field;
import net.therap.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

/**
 * @author shadman
 * @since 12/24/17
 */

@Service("bookingService")
public class BookingServiceImpl implements BookingService {

    @Autowired
    private BookingDao bookingDao;

    @Override
    public Booking getBookingById(long bookingId) {
        return bookingDao.getBookingById(bookingId);
    }

    @Override
    public void addBooking(Booking booking) {
        bookingDao.addBooking(booking);
    }

    @Override
    public List<Booking> getBookingsForSpecificFieldAndDay(Field fieldId, Date bookingDay) {
        return bookingDao.getBookingsForSpecificFieldAndDay(fieldId, bookingDay);
    }

    @Override
    public List<Booking> getAllBookings() {
        return bookingDao.getAllBookings();
    }

    @Override
    public List<Booking> getBookingsForSpecificUser(User userId, String bookingStatus) {
        return bookingDao.getBookingsForSpecificUser(userId, bookingStatus);
    }

    @Override
    public void confirmBooking(Booking booking) {
        bookingDao.confirmBooking(booking);
    }

    @Override
    public void rejectBooking(Booking booking) {
        bookingDao.rejectBooking(booking);
    }

    @Override
    public void cancelBooking(Booking booking) {
        bookingDao.cancelBooking(booking);
    }
}
