package net.therap.dao;

import net.therap.domain.Booking;
import net.therap.domain.Field;
import net.therap.domain.User;
import net.therap.helper.Constants;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

/**
 * @author shadman
 * @since 12/17/17
 */

@Repository("bookingDao")
public class BookingDaoImpl implements BookingDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void addBooking(Booking booking) {
        entityManager.persist(booking);
    }

    @Override
    public Booking getBookingById(long bookingId) {
        return entityManager.find(Booking.class, bookingId);
    }

    @Override
    public List<Booking> getBookingsForSpecificFieldAndDay(Field fieldId, Date bookingDay) {
        TypedQuery<Booking> typedQuery =
                entityManager.createQuery("SELECT b FROM Booking b WHERE fieldId = :fieldId AND " +
                                          "bookingDay = :bookingDay AND bookingStatus = :bookingStatus", Booking.class);
        typedQuery.setParameter("fieldId", fieldId);
        typedQuery.setParameter("bookingDay", bookingDay);
        typedQuery.setParameter("bookingStatus", Constants.BOOKING_STATUS_CONFIRMED);
        return typedQuery.getResultList();
    }

    @Override
    public List<Booking> getAllBookings() {
        Query query = entityManager.createQuery("FROM Booking ORDER BY fieldId, bookingDay");
        return query.getResultList();
    }

    @Override
    public List<Booking> getBookingsForSpecificUser(User userId, String bookingStatus) {
        TypedQuery<Booking> typedQuery;
        if (bookingStatus != null) {
            typedQuery =
                    entityManager.createQuery("SELECT b FROM Booking b WHERE userId = :userId AND " +
                                              "bookingStatus = :bookingStatus", Booking.class);
            typedQuery.setParameter("bookingStatus", bookingStatus);
        } else {
            typedQuery =
                    entityManager.createQuery("SELECT b FROM Booking b WHERE userId = :userId", Booking.class);
        }
        typedQuery.setParameter("userId", userId);
        return typedQuery.getResultList();
    }

    @Override
    @Transactional
    public void confirmBooking(Booking booking) {
        booking.setBookingStatus(Constants.BOOKING_STATUS_CONFIRMED);
        entityManager.merge(booking);
    }

    @Override
    @Transactional
    public void rejectBooking(Booking booking) {
        booking.setBookingStatus(Constants.BOOKING_STATUS_REJECTED);
        entityManager.merge(booking);
    }

    @Override
    @Transactional
    public void cancelBooking(Booking booking) {
        booking.setBookingStatus(Constants.BOOKING_STATUS_CANCELLED);
    }
}
