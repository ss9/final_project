package net.therap.dao;

import net.therap.domain.Booking;
import net.therap.domain.Field;
import net.therap.domain.User;

import java.sql.Date;
import java.util.List;

/**
 * @author shadman
 * @since 12/24/17
 */
public interface BookingDao {

    public Booking getBookingById(long bookingId);

    public void addBooking(Booking booking);

    public List<Booking> getBookingsForSpecificFieldAndDay(Field fieldId, Date bookingDay);

    public List<Booking> getAllBookings();

    public List<Booking> getBookingsForSpecificUser(User userId, String bookingStatus);

    public void confirmBooking(Booking booking);

    public void rejectBooking(Booking booking);

    public void cancelBooking(Booking booking);
}
