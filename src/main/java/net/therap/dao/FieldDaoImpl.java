package net.therap.dao;

import net.therap.domain.Field;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author shadman
 * @since 12/17/17
 */

@Repository("fieldDao")
public class FieldDaoImpl implements FieldDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void addField(Field field) {
        entityManager.persist(field);
    }

    @Override
    public Field getFieldById(long fieldId) {
        return entityManager.find(Field.class, fieldId);
    }

    @Override
    public List<Field> getAllFields() {
        Query query = entityManager.createQuery("FROM Field");
        return query.getResultList();
    }

    @Override
    @Transactional
    public void deleteField(Field field) {
        entityManager.remove(field);
    }
}
