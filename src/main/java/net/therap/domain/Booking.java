package net.therap.domain;

import net.therap.helper.Constants;

import javax.persistence.*;
import javax.validation.constraints.Future;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 * @author shadman
 * @since 12/17/17
 */

@Entity
@Table(name = "bookings")
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "booking_id")
    private long bookingId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;

    @ManyToOne
    @JoinColumn(name = "field_id")
    private Field fieldId;

    @Column(name = "booking_day")
    @Future
    private Date bookingDay;

    @Column(name = "booking_start_time")
    private Time startTime;

    @Column(name = "booking_end_time")
    private Time endTime;

    @Column(name = "booking_status")
    private String bookingStatus;

    public Booking() {
        this.bookingStatus = Constants.BOOKING_STATUS_PENDING;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Field getFieldId() {
        return fieldId;
    }

    public void setFieldId(Field fieldId) {
        this.fieldId = fieldId;
    }

    public Date getBookingDay() {
        return bookingDay;
    }

    public void setBookingDay(Date bookingDay) {
        this.bookingDay = bookingDay;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }
}
