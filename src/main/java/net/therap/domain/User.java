package net.therap.domain;

import net.therap.helper.Constants;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shadman
 * @since 12/17/17
 */

@Entity
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private long userId;

    @Column(name = "full_name")
    @NotEmpty
    @Size(min = 5, max = 30)
    @Pattern(regexp = "^[a-zA-Z]+( [a-zA-Z]+)*$")
    private String fullName;

    @Column
    @NotEmpty
    @Size(min = 3, max = 30)
    @Pattern(regexp = "^[0-9A-Za-z_.]*$")
    private String username;

    @Column
    @NotEmpty
    @Size(min = 5, max = 30)
    @Pattern(regexp = "^[0-9A-Za-z_.]*$")
    private String password;

    @Column(name = "user_type")
    private String userType;

    @Column
    @NotEmpty
    @Size(min = 5, max = 30)
    @Email
    private String email;

    @Column
    @NotEmpty
    @Size(min = 7, max = 15)
    @Digits(integer = 15, fraction = 0)
    private String phone;

    @OneToMany(mappedBy = "userId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings;

    public User() {
        this.userType = Constants.USER_TYPE_PUBLIC;
        this.bookings = new ArrayList<>();
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}
