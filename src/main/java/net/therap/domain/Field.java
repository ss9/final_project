package net.therap.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author shadman
 * @since 12/17/17
 */

@Entity
@Table(name = "fields")
public class Field implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "field_id")
    private long fieldId;

    @Column(name = "field_name")
    @NotEmpty
    @Size(min = 5, max = 40)
    @Pattern(regexp = "^[a-zA-Z0-9]+( [a-zA-Z0-9]+)*$")
    private String fieldName;

    @Column(name = "field_type")
    private String fieldType;

    @Column(name = "hourly_cost")
    @NotNull
    @Min(value = 100)
    @Max(value = 10000)
    private Integer hourlyCost;

    @Column(name = "max_people")
    @NotNull
    @Min(value = 2)
    @Max(value = 200)
    private Integer maxPeople;

    @Column(name = "rating")
    private Double rating;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "city", column = @Column(name = "field_city")),
            @AttributeOverride(name = "area", column = @Column(name = "field_area")),
            @AttributeOverride(name = "street", column = @Column(name = "field_street"))
    })
    @Valid
    private Address address;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "groundType", column = @Column(name = "field_ground_type")),
            @AttributeOverride(name = "grassType", column = @Column(name = "field_grass_type")),
            @AttributeOverride(name = "length", column = @Column(name = "field_length")),
            @AttributeOverride(name = "width", column = @Column(name = "field_width"))
    })
    @Valid
    private FieldDetails fieldDetails;

    @OneToMany(mappedBy = "fieldId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings;

    public Field() {

    }

    public long getFieldId() {
        return fieldId;
    }

    public void setFieldId(long fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public Integer getHourlyCost() {
        return hourlyCost;
    }

    public void setHourlyCost(Integer hourlyCost) {
        this.hourlyCost = hourlyCost;
    }

    public Integer getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(Integer maxPeople) {
        this.maxPeople = maxPeople;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public FieldDetails getFieldDetails() {
        return fieldDetails;
    }

    public void setFieldDetails(FieldDetails fieldDetails) {
        this.fieldDetails = fieldDetails;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}
