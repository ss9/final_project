package net.therap.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author shadman
 * @since 12/17/17
 */

@Embeddable
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    private String city;

    @NotEmpty
    @Size(min = 3, max = 30)
    @Pattern(regexp = "^[a-zA-Z0-9]+( [a-zA-Z0-9]+)*$")
    private String area;

    @NotEmpty
    @Size(min = 3, max = 30)
    @Pattern(regexp = "^[a-zA-Z0-9]+( [a-zA-Z0-9]+)*$")
    private String street;

    public Address() {

    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public String toString() {
        return this.street + ", " + this.area + ", " + this.city;
    }
}
