package net.therap.domain;

import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author shadman
 * @since 12/17/17
 */

@Embeddable
public class FieldDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    private String groundType;

    private String grassType;

    @NotNull
    @Min(value = 20)
    @Max(value = 500)
    private Integer length;

    @NotNull
    @Min(value = 20)
    @Max(value = 500)
    private Integer width;

    public FieldDetails() {

    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getGroundType() {
        return groundType;
    }

    public void setGroundType(String groundType) {
        this.groundType = groundType;
    }

    public String getGrassType() {
        return grassType;
    }

    public void setGrassType(String grassType) {
        this.grassType = grassType;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }
}
