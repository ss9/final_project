package net.therap.helper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shadman
 * @since 12/20/17
 */

public class Constants {

    public static final Integer RATING_MIN = 0;
    public static final Integer RATING_MAX = 5;

    public static final Integer RENTING_TIME_MIN = 0;
    public static final Integer RENTING_TIME_MAX = 24;

    public static final long HOUR_IN_MILLISECONDS = 60 * 60 * 1000;

    public static final String USER_TYPE_PUBLIC = "public";
    public static final String USER_TYPE_ADMIN = "admin";

    public static final String BOOKING_STATUS_PENDING = "pending";
    public static final String BOOKING_STATUS_CONFIRMED = "confirmed";
    public static final String BOOKING_STATUS_COMPLETED = "completed";
    public static final String BOOKING_STATUS_CANCELLED = "cancelled";
    public static final String BOOKING_STATUS_REJECTED = "rejected";

    public static final List<String> fieldTypes;
    public static final List<Integer> ratings;
    public static final List<String> cities;
    public static final List<String> groundTypes;
    public static final List<String> grassTypes;

    public static final List<Integer> rentingTimes;

    static {
        fieldTypes = new ArrayList<>();
        fieldTypes.add("Cricket");
        fieldTypes.add("Football");
        fieldTypes.add("Badminton");

        ratings = new ArrayList<>();
        for (Integer i = RATING_MIN; i <= RATING_MAX; i++) {
            ratings.add(i);
        }

        cities = new ArrayList<>();
        cities.add("Dhaka");
        cities.add("Chittagong");

        groundTypes = new ArrayList<>();
        groundTypes.add("Firm");
        groundTypes.add("Soft");
        groundTypes.add("Turf");
        groundTypes.add("Mat");

        grassTypes = new ArrayList<>();
        grassTypes.add("Artificial");
        grassTypes.add("None");
        grassTypes.add("Excellent");
        grassTypes.add("Here and there");

        rentingTimes = new ArrayList<>();
        for (Integer i = RENTING_TIME_MIN; i <= RENTING_TIME_MAX; i++) {
            rentingTimes.add(i);
        }
    }
}
