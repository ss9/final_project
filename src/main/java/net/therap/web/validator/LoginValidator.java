package net.therap.web.validator;

import net.therap.domain.User;
import net.therap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

/**
 * @author shadman
 * @since 12/18/17
 */

@Component
public class LoginValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;
        List<User> users = userService.getUsersByUsername(user.getUsername());
        if (users.isEmpty()) {
            errors.rejectValue("username", null, "Username doesn't exist");
            return;
        }
        String password = users.get(0).getPassword();
        if (!password.equals(user.getPassword())) {
            errors.rejectValue("password", null, "Password doesn't match");
        }
    }
}
