package net.therap.web.validator;

import net.therap.domain.User;
import net.therap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.List;

/**
 * @author shadman
 * @since 12/18/17
 */

@Component
@SessionAttributes("user")
public class RegistrationValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;
        List<User> users = userService.getUsersByUsername(user.getUsername());
        if (!users.isEmpty()) {
            errors.rejectValue("username", null, "Username exists");
        }
    }
}
