package net.therap.web.validator;

import net.therap.domain.Booking;
import net.therap.domain.Field;
import net.therap.helper.Constants;
import net.therap.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

/**
 * @author shadman
 * @since 12/21/17
 */

@Component
public class BookingValidator implements Validator {

    @Autowired
    private BookingService bookingService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Booking.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Booking booking = (Booking) target;
        Field field = booking.getFieldId();
        Date bookingDay = booking.getBookingDay();
        Time startTime = booking.getStartTime();
        Time endTime = booking.getEndTime();
        if (startTime == null) {
            errors.rejectValue("startTime", null, "Must specify a start time");
        }
        if (endTime == null) {
            errors.rejectValue("endTime", null, "Must specify an end time");
        }
        if (startTime == null || endTime == null) {
            return;
        }
        if (startTime.after(endTime)) {
            errors.rejectValue("startTime", null, "Start time must be before end time");
            errors.rejectValue("endTime", null, "End time must be after start time");
        }
        if ((endTime.getTime() - startTime.getTime()) < Constants.HOUR_IN_MILLISECONDS) {
            errors.rejectValue("startTime", null, "Must rent for at least an hour");
            errors.rejectValue("endTime", null, "Must rent for at least an hour");
        }
        List<Booking> bookingsForSpecificFieldAndDay =
                bookingService.getBookingsForSpecificFieldAndDay(field, bookingDay);

        for (Booking bookingForSpecificFieldAndDay : bookingsForSpecificFieldAndDay) {
            Time bookingStartTime = bookingForSpecificFieldAndDay.getStartTime();
            Time bookingEndTime = bookingForSpecificFieldAndDay.getEndTime();
            if ((startTime.after(bookingStartTime) && startTime.before(bookingEndTime)) ||
                    endTime.after(bookingStartTime) && endTime.before(bookingEndTime)) {
                errors.rejectValue("bookingDay", null, "This field is not available on this day and time");
                errors.rejectValue("startTime", null, "This field is not available on this day and time");
                errors.rejectValue("endTime", null, "This field is not available on this day and time");
                break;
            }
        }
    }
}
