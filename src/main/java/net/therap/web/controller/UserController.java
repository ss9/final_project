package net.therap.web.controller;

import net.therap.domain.Booking;
import net.therap.domain.User;
import net.therap.helper.Constants;
import net.therap.service.BookingService;
import net.therap.service.UserService;
import net.therap.web.validator.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author shadman
 * @since 12/19/17
 */

@Controller
@SessionAttributes("user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private RegistrationValidator registrationValidator;

    @ModelAttribute
    public void addAttributes(Model model, HttpSession httpSession) {
        User sessionUser = (User) httpSession.getAttribute("user");
        model.addAttribute("userObject", sessionUser);
        model.addAttribute("histories",
                bookingService.getBookingsForSpecificUser(sessionUser, null));
        model.addAttribute("matchdays",
                bookingService.getBookingsForSpecificUser(sessionUser, Constants.BOOKING_STATUS_CONFIRMED));
        model.addAttribute("pendingBookings",
                bookingService.getBookingsForSpecificUser(sessionUser, Constants.BOOKING_STATUS_PENDING));
        model.addAttribute("rejectedBookings",
                bookingService.getBookingsForSpecificUser(sessionUser, Constants.BOOKING_STATUS_REJECTED));
        model.addAttribute("cancelledBookings",
                bookingService.getBookingsForSpecificUser(sessionUser, Constants.BOOKING_STATUS_CANCELLED));
        model.addAttribute("completedBookings",
                bookingService.getBookingsForSpecificUser(sessionUser, Constants.BOOKING_STATUS_COMPLETED));
    }

    @RequestMapping(value = "/private/user/UpdateSingleFieldPublicController", method = RequestMethod.POST)
    public String updateSingleField(@Valid @ModelAttribute("userObject") User userObject,
                                    BindingResult bindingResult, Model model) {
        User user = userService.getUserById(userObject.getUserId());
        model.addAttribute("user", user);
        if (!user.getUsername().equals(userObject.getUsername())) {
            registrationValidator.validate(userObject, bindingResult);
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("updateValidationError", true);
            return "user/profile";
        }
        userService.updateUser(userService.getUserById(userObject.getUserId()), userObject);
        model.addAttribute("user", userService.getUserById(userObject.getUserId()));
        return "user/profile";
    }

    @RequestMapping(value = "/private/user/UpdateSingleFieldPublicController", method = RequestMethod.GET)
    public String goToIndexFromUpdateSingleFieldPublic() {
        return "general/index";
    }

    @RequestMapping(value = "/private/user/ProfileController")
    public String goToProfile() {
        return "user/profile";
    }

    @RequestMapping(value = "/private/UserDetailsController")
    public String viewUserDetails(Model model, @RequestParam(value = "userToView") User userToView) {
        model.addAttribute("userToView", userToView);
        model.addAttribute("histories",
                bookingService.getBookingsForSpecificUser(userToView, null));
        model.addAttribute("matchdays",
                bookingService.getBookingsForSpecificUser(userToView, Constants.BOOKING_STATUS_CONFIRMED));
        model.addAttribute("pendingBookings",
                bookingService.getBookingsForSpecificUser(userToView, Constants.BOOKING_STATUS_PENDING));
        model.addAttribute("rejectedBookings",
                bookingService.getBookingsForSpecificUser(userToView, Constants.BOOKING_STATUS_REJECTED));
        model.addAttribute("cancelledBookings",
                bookingService.getBookingsForSpecificUser(userToView, Constants.BOOKING_STATUS_CANCELLED));
        model.addAttribute("completedBookings",
                bookingService.getBookingsForSpecificUser(userToView, Constants.BOOKING_STATUS_COMPLETED));
        return "user/profile";
    }

    @RequestMapping(value = "/private/user/CancelBookingController", method = RequestMethod.POST)
    public String cancelBooking(@ModelAttribute("bookingObject") Booking bookingObject) {
        Booking booking = bookingService.getBookingById(bookingObject.getBookingId());
        bookingService.cancelBooking(booking);
        return "user/profile";
    }

    @RequestMapping(value = "/private/user/CancelBookingController", method = RequestMethod.GET)
    public String goToIndexFromCancelBooking() {
        return "general/index";
    }
}
