package net.therap.web.controller;

import net.therap.domain.Booking;
import net.therap.domain.Field;
import net.therap.service.BookingService;
import net.therap.service.FieldService;
import net.therap.web.validator.BookingValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author shadman
 * @since 12/20/17
 */

@Controller
@SessionAttributes("user")
public class FieldController {

    @Autowired
    private FieldService fieldService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingValidator bookingValidator;

    @RequestMapping(value = "/private/admin/AddFieldController", method = RequestMethod.POST)
    public String addField(@Valid @ModelAttribute("fieldObject") Field fieldObject,
                           BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("addFieldValidationError", true);
            return "user/home";
        }
        fieldService.addField(fieldObject);
        model.addAttribute("fields", fieldService.getAllFields());
        return "user/home";
    }

    @RequestMapping(value = "/private/admin/AddFieldController", method = RequestMethod.GET)
    public String goToIndexFromAddField() {
        return "general/index";
    }

    @RequestMapping(value = "/private/admin/DeleteFieldController", method = RequestMethod.POST)
    public String deleteField(@ModelAttribute("fieldObject") Field fieldObject, Model model) {
        fieldService.deleteField(fieldService.getFieldById(fieldObject.getFieldId()));
        model.addAttribute("fields", fieldService.getAllFields());
        return "user/home";
    }

    @RequestMapping(value = "/private/admin/DeleteFieldController", method = RequestMethod.GET)
    public String goToIndexFromDeleteField() {
        return "general/index";
    }

    @RequestMapping(value = "/private/user/RequestToRentFieldController", method = RequestMethod.POST)
    public String rentField(@ModelAttribute("fieldObject") Field fieldObject, Model model) {
        model.addAttribute("fieldToRent", fieldService.getFieldById(fieldObject.getFieldId()));
        return "user/home";
    }

    @RequestMapping(value = "/private/user/RequestToRentFieldController", method = RequestMethod.GET)
    public String goToIndexFromRequestToRentField() {
        return "general/index";
    }

    @RequestMapping(value = "/private/user/RentFieldController", method = RequestMethod.POST)
    public String rentField(@Valid @ModelAttribute("bookingObject") Booking bookingObject,
                            BindingResult bindingResult, Model model) {
        bookingValidator.validate(bookingObject, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("fieldToRent", bookingObject.getFieldId());
            model.addAttribute("rentFieldValidationError", true);
            return "user/home";
        }
        bookingService.addBooking(bookingObject);
        return "user/home";
    }

    @RequestMapping(value = "/private/user/RentFieldController", method = RequestMethod.GET)
    public String goToIndexFromRentField() {
        return "general/index";
    }

    @RequestMapping(value = "/private/FieldDetailsController")
    public String viewFieldDetails(Model model, @RequestParam(value = "fieldToView") Field fieldToView) {
        model.addAttribute("fieldToView", fieldToView);
        return "user/field_details";
    }
}
