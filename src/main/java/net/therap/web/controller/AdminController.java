package net.therap.web.controller;

import net.therap.domain.Booking;
import net.therap.domain.User;
import net.therap.service.BookingService;
import net.therap.service.UserService;
import net.therap.web.validator.BookingValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @author shadman
 * @since 12/24/17
 */

@Controller
public class AdminController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private UserService userService;

    @Autowired
    private BookingValidator bookingValidator;

    @RequestMapping(value = "/private/admin/BookingsController")
    public String goToBookings() {
        return "admin/bookings";
    }

    @RequestMapping(value = "/private/admin/ConfirmBookingController", method = RequestMethod.POST)
    public String confirmBooking(@Valid @ModelAttribute("bookingObject") Booking bookingObject,
                                 BindingResult bindingResult, Model model) {
        bookingValidator.validate(bookingObject, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("bookingConfirmationError", true);
            return "admin/bookings";
        }
        bookingService.confirmBooking(bookingService.getBookingById(bookingObject.getBookingId()));
        return "admin/bookings";
    }

    @RequestMapping(value = "/private/admin/ConfirmBookingController", method = RequestMethod.GET)
    public String goToIndexFromConfirmBooking() {
        return "general/index";
    }

    @RequestMapping(value = "/private/admin/RejectBookingController", method = RequestMethod.POST)
    public String rejectBooking(@ModelAttribute("bookingObject") Booking bookingObject) {
        Booking booking = bookingService.getBookingById(bookingObject.getBookingId());
        bookingService.rejectBooking(booking);
        return "admin/bookings";
    }

    @RequestMapping(value = "/private/admin/RejectBookingController", method = RequestMethod.GET)
    public String goToIndexFromRejectBooking() {
        return "general/index";
    }

    @RequestMapping(value = "/private/admin/CancelBookingController", method = RequestMethod.POST)
    public String cancelBooking(@ModelAttribute("bookingObject") Booking bookingObject) {
        Booking booking = bookingService.getBookingById(bookingObject.getBookingId());
        bookingService.cancelBooking(booking);
        return "admin/bookings";
    }

    @RequestMapping(value = "/private/admin/CancelBookingController", method = RequestMethod.GET)
    public String goToIndexFromCancelBooking() {
        return "general/index";
    }

    @RequestMapping(value = "/private/admin/ManageUsersController")
    public String goToManageUsers() {
        return "admin/users";
    }

    @RequestMapping(value = "/private/admin/DeleteUserController", method = RequestMethod.POST)
    public String deleteUser(@ModelAttribute("userObject") User userObject, Model model) {
        User user = userService.getUserById(userObject.getUserId());
        userService.deleteUser(user);
        model.addAttribute("users", userService.getAllUsers());
        return "admin/users";
    }

    @RequestMapping(value = "/private/admin/DeleteUserController", method = RequestMethod.GET)
    public String goToIndexFromDeleteUser() {
        return "general/index";
    }
}
