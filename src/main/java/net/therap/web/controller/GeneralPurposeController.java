package net.therap.web.controller;

import net.therap.domain.User;
import net.therap.service.UserService;
import net.therap.web.validator.LoginValidator;
import net.therap.web.validator.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;

/**
 * @author shadman
 * @since 12/18/17
 */

@Controller
@SessionAttributes("user")
public class GeneralPurposeController {

    @Autowired
    private UserService userService;

    @Autowired
    private RegistrationValidator registrationValidator;

    @Autowired
    private LoginValidator loginValidator;

    @RequestMapping(value = "/FaqController")
    public String goToFaq() {
        return "general/faq";
    }

    @RequestMapping(value = "/RegistrationController", method = RequestMethod.POST)
    public String registerUser(@Valid @ModelAttribute("userObjectRegistration") User userObjectRegistration,
                               BindingResult bindingResult, Model model) {
        registrationValidator.validate(userObjectRegistration, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("registrationValidationError", true);
            return "general/index";
        }
        userService.addUser(userObjectRegistration);
        model.addAttribute("userObjectRegistration", new User());
        return "general/index";
    }

    @RequestMapping(value = "/RegistrationController", method = RequestMethod.GET)
    public String goToIndexFromRegistration() {
        return "general/index";
    }

    @RequestMapping(value = "/LoginController", method = RequestMethod.POST)
    public String loginUser(@Valid @ModelAttribute("userObjectLogin") User userObjectLogin,
                            BindingResult bindingResult, Model model) {
        loginValidator.validate(userObjectLogin, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("loginValidationError", true);
            return "general/index";
        }
        User user = userService.getUsersByUsername(userObjectLogin.getUsername()).get(0);
        model.addAttribute("user", user);
        return "user/home";
    }

    @RequestMapping(value = "/LoginController", method = RequestMethod.GET)
    public String goToIndexFromLogin() {
        return "general/index";
    }

    @RequestMapping(value = "/private/LogoutController", method = RequestMethod.POST)
    public String logoutUser(SessionStatus sessionStatus) {
        sessionStatus.setComplete();
        return "general/index";
    }

    @RequestMapping(value = "/private/LogoutController", method = RequestMethod.GET)
    public String goToIndexFromLogout() {
        return "general/index";
    }

    @RequestMapping(value = "/private/HomeController")
    public String goToHome() {
        return "user/home";
    }
}
