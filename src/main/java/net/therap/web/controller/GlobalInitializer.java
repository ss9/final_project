package net.therap.web.controller;

import net.therap.domain.Booking;
import net.therap.domain.Field;
import net.therap.domain.User;
import net.therap.helper.Constants;
import net.therap.service.BookingService;
import net.therap.service.FieldService;
import net.therap.service.UserService;
import net.therap.web.editor.BookingEditor;
import net.therap.web.editor.FieldEditor;
import net.therap.web.editor.UserEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * @author shadman
 * @since 12/25/17
 */

@ControllerAdvice
public class GlobalInitializer {

    @Autowired
    private UserService userService;

    @Autowired
    private FieldService fieldService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private UserEditor userEditor;

    @Autowired
    private FieldEditor fieldEditor;

    @Autowired
    private BookingEditor bookingEditor;

    @ModelAttribute
    public void setAttributes(Model model) {
        model.addAttribute("userObject", new User());
        model.addAttribute("userObjectLogin", new User());
        model.addAttribute("userObjectRegistration", new User());
        model.addAttribute("users", userService.getAllUsers());

        model.addAttribute("fieldObject", new Field());
        model.addAttribute("fields", fieldService.getAllFields());

        model.addAttribute("bookingObject", new Booking());
        model.addAttribute("bookings", bookingService.getAllBookings());

        model.addAttribute("ratings", Constants.ratings);
        model.addAttribute("cities", Constants.cities);
        model.addAttribute("groundTypes", Constants.groundTypes);
        model.addAttribute("grassTypes", Constants.grassTypes);
        model.addAttribute("fieldTypes", Constants.fieldTypes);
        model.addAttribute("rentingTimes", Constants.rentingTimes);
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(User.class, userEditor);
        webDataBinder.registerCustomEditor(Field.class, fieldEditor);
        webDataBinder.registerCustomEditor(Booking.class, bookingEditor);
    }
}
