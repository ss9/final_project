package net.therap.web.editor;

import net.therap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * @author shadman
 * @since 12/20/17
 */

@Component
public class UserEditor extends PropertyEditorSupport {

    @Autowired
    private UserService userService;

    @Override
    public void setAsText(String userId) throws IllegalArgumentException {
        setValue(userService.getUserById(Long.parseLong(userId)));
    }
}
