package net.therap.web.editor;

import net.therap.service.FieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * @author shadman
 * @since 12/20/17
 */

@Component
public class FieldEditor extends PropertyEditorSupport {

    @Autowired
    private FieldService fieldService;

    @Override
    public void setAsText(String fieldId) throws IllegalArgumentException {
        setValue(fieldService.getFieldById(Long.parseLong(fieldId)));
    }
}
