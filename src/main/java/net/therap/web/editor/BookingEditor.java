package net.therap.web.editor;

import net.therap.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * @author shadman
 * @since 12/25/17
 */

@Component
public class BookingEditor extends PropertyEditorSupport {

    @Autowired
    private BookingService bookingService;

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        setValue(bookingService.getBookingById(Long.parseLong(text)));
    }
}
