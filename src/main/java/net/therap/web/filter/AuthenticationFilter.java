package net.therap.web.filter;

import net.therap.domain.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 12/19/17
 */

@WebFilter(urlPatterns = {"/private/*"})
public class AuthenticationFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            User user = (User) httpServletRequest.getSession().getAttribute("user");
            httpServletResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            httpServletResponse.setHeader("Pragma", "no-cache");
            httpServletResponse.setDateHeader("Expires", 0);
            if (user == null) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/general/index.jsp");
                requestDispatcher.forward(request, response);
            } else {
                if (!user.getUserType().trim().equals("public") &&
                        httpServletRequest.getRequestURL().toString().contains("/user/")) {
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/user/home.jsp");
                    requestDispatcher.forward(request, response);
                }
                if (!user.getUserType().trim().equals("admin") &&
                        httpServletRequest.getRequestURL().toString().contains("/admin/")) {
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/user/home.jsp");
                    requestDispatcher.forward(request, response);
                }
            }
            filterChain.doFilter(request, response);
        }
    }

    public void destroy() {

    }

    public void init(FilterConfig filterConfig) throws ServletException {

    }
}

