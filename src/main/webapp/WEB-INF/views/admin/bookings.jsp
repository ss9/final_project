<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:import url="/WEB-INF/views/common/head.jsp"/>
    <title>Bookings</title>
    <script src="<c:url value="/js/bookings.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/css/general.css"/>" />
    <link rel="stylesheet" href="<c:url value="/css/bookings.css"/>" />
</head>
<body>
<div class="container">

    <c:set var="confirmBookingController"><c:url value="/private/admin/ConfirmBookingController"/></c:set>
    <c:set var="rejectBookingController"><c:url value="/private/admin/RejectBookingController"/></c:set>
    <c:set var="cancelBookingController"><c:url value="/private/admin/CancelBookingController"/></c:set>

    <c:import url="/WEB-INF/views/common/navbar.jsp"/>
    <c:import url="/WEB-INF/views/modal/booking_confirmation_error_modal.jsp"/>

    <ul class = "nav nav-tabs">
        <li class = "active"><a href = "#activeBookings" data-toggle = "tab">Active Bookings</a>
        <li><a href = "#deadBookings" data-toggle = "tab">Dead Bookings</a>
    </ul>

    <div class = "tab-content">
        <div class = "tab-pane fade in active" id = "activeBookings">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Day</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Booked By</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${bookings}" var="booking">
                        <c:if test="${booking.bookingStatus == 'pending' || booking.bookingStatus == 'confirmed'}">
                            <tr>
                                <td>
                                    <a href="
                                        <c:url value="/private/FieldDetailsController">
                                            <c:param name="fieldToView" value="${booking.fieldId.fieldId}"/>
                                        </c:url>"><c:out value="${booking.fieldId.fieldName}"/>
                                    </a>
                                </td>
                                <td><c:out value="${booking.bookingDay}"/></td>
                                <td><c:out value="${booking.startTime}"/></td>
                                <td><c:out value="${booking.endTime}"/></td>
                                <td>
                                    <a href="
                                        <c:url value="/private/UserDetailsController">
                                            <c:param name="userToView" value="${booking.userId.userId}"/>
                                        </c:url>"><c:out value="${booking.userId.username}"/>
                                    </a>
                                </td>
                                <td><c:out value="${booking.bookingStatus}"/></td>
                                <td>
                                    <c:if test="${booking.bookingStatus == 'pending'}">
                                        <sf:form action="${confirmBookingController}" method="post"
                                                 modelAttribute="bookingObject" class="inlineForm">
                                            <sf:hidden path="bookingId" value="${booking.bookingId}"/>
                                            <sf:hidden path="fieldId" value="${booking.fieldId.fieldId}"/>
                                            <sf:hidden path="bookingDay" value="${booking.bookingDay}"/>
                                            <sf:hidden path="startTime" value="${booking.startTime}"/>
                                            <sf:hidden path="endTime" value="${booking.endTime}"/>
                                            <button type="submit" class="btn btn-success btn-sm">Accept</button>
                                        </sf:form>
                                        <sf:form action="${rejectBookingController}" method="post"
                                                 modelAttribute="bookingObject" class="inlineForm">
                                            <sf:hidden path="bookingId" value="${booking.bookingId}"/>
                                            <button type="submit" class="btn btn-danger btn-sm">Reject</button>
                                        </sf:form>
                                    </c:if>
                                    <c:if test="${booking.bookingStatus == 'confirmed'}">
                                        <sf:form action="${cancelBookingController}" method="post"
                                                 modelAttribute="bookingObject" class="inlineForm">
                                            <sf:hidden path="bookingId" value="${booking.bookingId}"/>
                                            <button type="submit" class="btn btn-danger btn-sm">Cancel</button>
                                        </sf:form>
                                    </c:if>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class = "tab-pane fade in" id = "deadBookings">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Day</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Booked By</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${bookings}" var="booking">
                        <c:if test="${booking.bookingStatus != 'pending' && booking.bookingStatus != 'confirmed'}">
                            <tr>
                                <td>
                                    <a href="
                                <c:url value="/private/FieldDetailsController">
                                    <c:param name="fieldToView" value="${booking.fieldId.fieldId}"/>
                                </c:url>"><c:out value="${booking.fieldId.fieldName}"/>
                                    </a>
                                </td>
                                <td><c:out value="${booking.bookingDay}"/></td>
                                <td><c:out value="${booking.startTime}"/></td>
                                <td><c:out value="${booking.endTime}"/></td>
                                <td>
                                    <a href="
                                <c:url value="/private/UserDetailsController">
                                    <c:param name="userToView" value="${booking.userId.userId}"/>
                                </c:url>"><c:out value="${booking.userId.username}"/>
                                    </a>
                                </td>
                                <td><c:out value="${booking.bookingStatus}"/></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <c:if test="${bookingConfirmationError == true}">
        <script>
            $('#bookingConfirmationErrorModal').modal('show');
        </script>
    </c:if>
    <c:import url="/WEB-INF/views/common/footer.jsp"/>
</div>
</body>
</html>
