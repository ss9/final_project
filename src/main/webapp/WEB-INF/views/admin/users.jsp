<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:import url="/WEB-INF/views/common/head.jsp"/>
    <title>Manage Users</title>
    <script src="<c:url value="/js/users.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/css/general.css"/>" />
    <link rel="stylesheet" href="<c:url value="/css/users.css"/>" />
</head>
<body>
<div class="container">

    <c:set var="deleteUserController"><c:url value="/private/admin/DeleteUserController"/></c:set>

    <c:import url="/WEB-INF/views/common/navbar.jsp"/>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Full Name</th>
                <th>Username</th>
                <th>Total Bookings</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${users}" var="managedUser">
                <tr>
                    <td><c:out value="${managedUser.fullName}"/></td>
                    <td>
                        <a href="
                            <c:url value="/private/UserDetailsController">
                                <c:param name="userToView" value="${managedUser.userId}"/>
                            </c:url>"><c:out value="${managedUser.username}"/>
                        </a>
                    </td>
                    <td><c:out value="${fn:length(managedUser.bookings)}"/></td>
                    <td>
                        <sf:form action="${deleteUserController}" method="post" modelAttribute="userObject">
                            <sf:hidden path="userId" value="${managedUser.userId}"/>
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </sf:form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <c:import url="/WEB-INF/views/common/footer.jsp"/>
</div>
</body>
</html>
