<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:import url="/WEB-INF/views/common/head.jsp"/>
    <title>Field Details</title>
    <script src="<c:url value="/js/field_details.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/css/general.css"/>" />
    <link rel="stylesheet" href="<c:url value="/css/field_details.css"/>" />
</head>
<body>
<div class="container">

    <c:set var="deleteFieldController"><c:url value="/private/admin/DeleteFieldController"/></c:set>
    <c:set var="rentFieldController"><c:url value="/private/user/RequestToRentFieldController"/></c:set>

    <c:import url="/WEB-INF/views/common/navbar.jsp"/>

    <h1 class="text-center">IMAGE GOES HERE</h1>

    <c:if test="${sessionScope.user.userType=='admin'}">
        <sf:form action="${deleteFieldController}" method="post" modelAttribute="fieldObject">
            <sf:hidden path="fieldId" value="${fieldToView.fieldId}"/>
            <button type="submit" class="btn btn-block btn-danger">Delete</button>
        </sf:form>
    </c:if>
    <c:if test="${sessionScope.user.userType=='public'}">
        <sf:form action="${rentFieldController}" method="post" modelAttribute="fieldObject">
            <sf:hidden path="fieldId" value="${fieldToView.fieldId}"/>
            <button type="submit" class="btn btn-block btn-primary">Rent</button>
        </sf:form>
    </c:if>

    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h2 class="text-info">Main Attributes</h2><hr>
                <h4>Name: <c:out value="${fieldToView.fieldName}"/></h4><hr>
                <h4>Type: <c:out value="${fieldToView.fieldType}"/></h4><hr>
                <h4>Hourly Cost: <c:out value="${fieldToView.hourlyCost}"/></h4><hr>
                <h4>Max. Allowed People: <c:out value="${fieldToView.maxPeople}"/></h4><hr>
                <h4>Rating: <c:out value="${fieldToView.rating}"/></h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="jumbotron">
                <h2 class="text-info">Address</h2><hr>
                <h4>City: <c:out value="${fieldToView.address.city}"/></h4><hr>
                <h4>Area: <c:out value="${fieldToView.address.area}"/></h4><hr>
                <h4>Street: <c:out value="${fieldToView.address.street}"/></h4>
            </div>
        </div>
        <div class="col-md-6">
            <div class="jumbotron">
                <h2 class="text-info">Field Details</h2><hr>
                <h4>Grass Type: <c:out value="${fieldToView.fieldDetails.grassType}"/></h4><hr>
                <h4>Ground Type: <c:out value="${fieldToView.fieldDetails.groundType}"/></h4><hr>
                <h4>Length: <c:out value="${fieldToView.fieldDetails.length}"/>,
                    Width: <c:out value="${fieldToView.fieldDetails.width}"/></h4>
            </div>
        </div>
    </div>
    <c:import url="/WEB-INF/views/common/footer.jsp"/>
</div>
</body>
</html>
