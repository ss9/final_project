<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:import url="/WEB-INF/views/common/head.jsp"/>
    <title>Profile</title>
    <script src="<c:url value="/js/profile.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/css/general.css"/>" />
    <link rel="stylesheet" href="<c:url value="/css/profile.css"/>" />
</head>
<body>
<div class="container">
    <c:set var="updateSingleFieldPublicController"><c:url value="/private/user/UpdateSingleFieldPublicController"/></c:set>
    <c:set var="cancelBookingController"><c:url value="/private/user/CancelBookingController"/></c:set>

    <c:import url="/WEB-INF/views/common/navbar.jsp"/>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3>PICTURE GOES HERE</h3>
                </div>
                <div class="col-md-8">
                    <h4 class="text-info">Full Name</h4>
                    <h4>
                        <c:if test="${userToView != null}">
                            <c:out value="${userToView.fullName}"/>
                        </c:if>
                        <c:if test="${userToView == null}">
                            <c:out value="${sessionScope.user.fullName}"/>
                            <button type="button" class="btn btn-sm editButton">Edit</button>
                            <div class="input-group editSection"><br>
                                <sf:form action="${updateSingleFieldPublicController}" method="post"
                                         modelAttribute="userObject">
                                        <sf:errors path="fullName" cssClass="error" element="div"/>
                                        <sf:input path="fullName"/>
                                        <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                </sf:form>
                            </div>
                        </c:if>
                    </h4>
                    <h4 class="text-info">Username</h4>
                    <h4>
                        <c:if test="${userToView != null}">
                            <c:out value="${userToView.username}"/>
                        </c:if>
                        <c:if test="${userToView == null}">
                            <c:out value="${sessionScope.user.username}"/>
                            <button type="button" class="btn btn-sm editButton">Edit</button>
                            <div class="input-group editSection"><br>
                                <sf:form action="${updateSingleFieldPublicController}" method="post"
                                         modelAttribute="userObject">
                                    <sf:errors path="username" cssClass="error" element="div"/>
                                    <sf:input path="username"/>
                                    <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                </sf:form>
                            </div>
                        </c:if>
                    </h4>
                    <h4 class="text-info">Email</h4>
                    <h4>
                        <c:if test="${userToView != null}">
                            <c:out value="${userToView.email}"/>
                        </c:if>
                        <c:if test="${userToView == null}">
                            <c:out value="${sessionScope.user.email}"/>
                            <button type="button" class="btn btn-sm editButton">Edit</button>
                            <div class="input-group editSection"><br>
                                <sf:form action="${updateSingleFieldPublicController}" method="post"
                                         modelAttribute="userObject">
                                    <sf:errors path="email" cssClass="error" element="div"/>
                                    <sf:input path="email"/>
                                    <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                </sf:form>
                            </div>
                        </c:if>
                    </h4>
                    <h4 class="text-info">Phone</h4>
                    <h4>
                        <c:if test="${userToView != null}">
                            <c:out value="${userToView.phone}"/>
                        </c:if>
                        <c:if test="${userToView == null}">
                            <c:out value="${sessionScope.user.phone}"/>
                            <button type="button" class="btn btn-sm editButton">Edit</button>
                            <div class="input-group editSection"><br>
                                <sf:form action="${updateSingleFieldPublicController}" method="post"
                                         modelAttribute="userObject">
                                    <sf:errors path="phone" cssClass="error" element="div"/>
                                    <sf:input path="phone"/>
                                    <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                </sf:form>
                            </div>
                        </c:if>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.editSection').hide();
    </script>
    <c:if test="${updateValidationError == true}">
        <script>
            $('.editSection').show();
        </script>
    </c:if>
    <c:if test="${userToView == null}">
        <script>
            $('#profilePublic').addClass('active');
        </script>
    </c:if>
    <ul class = "nav nav-tabs">
        <li class = "active"><a href = "#matchdays" data-toggle = "tab">Matchdays</a>
        <li><a href = "#history" data-toggle = "tab">History</a>
        <li><a href = "#stats" data-toggle = "tab">Stats</a>
    </ul>
    <div class = "tab-content">
        <div class = "tab-pane fade in active" id = "matchdays">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Day</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <c:if test="${userToView == null}">
                            <th>Action</th>
                        </c:if>
                    </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${matchdays}" var="matchday">
                            <c:if test="${matchday.bookingStatus == 'confirmed'}">
                                <tr>
                                    <td><c:out value="${matchday.fieldId.fieldName}"/></td>
                                    <td><c:out value="${matchday.bookingDay}"/></td>
                                    <td><c:out value="${matchday.startTime}"/></td>
                                    <td><c:out value="${matchday.endTime}"/></td>
                                    <c:if test="${userToView == null}">
                                        <td>
                                            <sf:form action="${cancelBookingController}" method="post"
                                                     modelAttribute="bookingObject" class="inlineForm">
                                                <sf:hidden path="bookingId" value="${matchday.bookingId}"/>
                                                <button type="submit" class="btn btn-danger btn-sm">Cancel</button>
                                            </sf:form>
                                        </td>
                                    </c:if>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class = "tab-pane fade in" id = "history">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Day</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${histories}" var="history">
                        <c:if test="${history.bookingStatus != 'confirmed'}">
                            <tr>
                                <td>
                                    <a href="
                                    <c:url value="/private/FieldDetailsController">
                                        <c:param name="fieldToView" value="${history.fieldId.fieldId}"/>
                                    </c:url>"><c:out value="${history.fieldId.fieldName}"/>
                                    </a>
                                </td>
                                <td><c:out value="${history.bookingDay}"/></td>
                                <td><c:out value="${history.startTime}"/></td>
                                <td><c:out value="${history.endTime}"/></td>
                                <td><c:out value="${history.bookingStatus}"/></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class = "tab-pane fade in" id = "stats">
            <hr>
            <div class="jumbotron">
                <h2>Total Bookings: <c:out value="${fn:length(histories)}"/></h2>
                <hr>
                <ul>
                    <li>
                        <h4 class="text-info">Completed: <c:out value="${fn:length(completedBookings)}"/></h4>
                    </li>
                    <li>
                        <h4 class="text-info">Confirmed: <c:out value="${fn:length(matchdays)}"/></h4>
                    </li>
                    <li>
                        <h4 class="text-info">Pending: <c:out value="${fn:length(pendingBookings)}"/></h4>
                    </li>
                    <li>
                        <h4 class="text-info">Rejected: <c:out value="${fn:length(rejectedBookings)}"/></h4>
                    </li>
                    <li>
                        <h4 class="text-info">Cancelled: <c:out value="${fn:length(cancelledBookings)}"/></h4>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <c:import url="/WEB-INF/views/common/footer.jsp"/>
</div>
</body>
</html>
