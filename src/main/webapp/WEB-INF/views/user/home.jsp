<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:import url="/WEB-INF/views/common/head.jsp"/>
    <title>Browse Fields</title>
    <script src="<c:url value="/js/home.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/css/general.css"/>" />
    <link rel="stylesheet" href="<c:url value="/css/home.css"/>" />
</head>
<body>
<div class="container">
    <c:set var="addFieldController"><c:url value="/private/admin/AddFieldController"/></c:set>
    <c:set var="deleteFieldController"><c:url value="/private/admin/DeleteFieldController"/></c:set>
    <c:set var="rentFieldController"><c:url value="/private/user/RequestToRentFieldController"/></c:set>
    <c:set var="fieldDetailsController"><c:url value="/private/FieldDetailsController"/></c:set>

    <c:import url="/WEB-INF/views/common/navbar.jsp"/>

    <c:if test="${sessionScope.user.userType=='admin'}">
        <button class = "btn btn-block btn-success"
                id = "addFieldButton">Add Field <span class = "caret"></span></button><br>
        <sf:form id = "addFieldForm" modelAttribute="fieldObject"
                 action="${addFieldController}" method="post">
            <div class = "form-group">
                <sf:label path="fieldName">Field Name</sf:label>
                <sf:errors path="fieldName" element="div" cssClass="error"/>
                <sf:input path="fieldName" class = "form-control" id = "fieldNameAddField"/>
            </div>
            <div class = "form-group row">
                <div class="col-lg-3">
                    <sf:label path="fieldType">Field Type</sf:label>
                    <sf:select path="fieldType" items="${fieldTypes}" class = "form-control" id = "fieldTypeAddField"/>
                    <sf:errors path="fieldType" element="div" cssClass="error"/>
                </div>
                <div class="col-lg-3">
                    <sf:label path="hourlyCost">Hourly Cost</sf:label>
                    <sf:input path="hourlyCost" class = "form-control" id = "hourlyCostAddField"/>
                    <sf:errors path="hourlyCost" element="div" cssClass="error"/>
                </div>
                <div class="col-lg-3">
                    <sf:label path="maxPeople">Maximum Allowed People</sf:label>
                    <sf:input path="maxPeople" class = "form-control" id = "maxPeopleAddField"/>
                    <sf:errors path="maxPeople" element="div" cssClass="error"/>
                </div>
                <div class="col-lg-3">
                    <sf:label path="rating">Rating</sf:label>
                    <sf:select path="rating" items="${ratings}" class = "form-control" id = "ratingAddField"/>
                    <sf:errors path="rating" element="div" cssClass="error"/>
                </div>
            </div>
            <div class = "form-group row">
                <div class="col-lg-4">
                    <sf:label path="address.city">City</sf:label>
                    <sf:select path="address.city" items="${cities}" class = "form-control" id = "cityAddField"/>
                    <sf:errors path="address.city" element="div" cssClass="error"/>
                </div>
                <div class="col-lg-4">
                    <sf:label path="address.area">Area</sf:label>
                    <sf:input path="address.area" class = "form-control" id = "areaAddField"/>
                    <sf:errors path="address.area" element="div" cssClass="error"/>
                </div>
                <div class="col-lg-4">
                    <sf:label path="address.street">Street</sf:label>
                    <sf:input path="address.street" class = "form-control" id = "streetAddField"/>
                    <sf:errors path="address.street" element="div" cssClass="error"/>
                </div>
            </div>
            <div class = "form-group row">
                <div class="col-lg-3">
                    <sf:label path="fieldDetails.groundType">Ground Type</sf:label>
                    <sf:select path="fieldDetails.groundType" items="${groundTypes}" class = "form-control" id = "groundypeAddField"/>
                    <sf:errors path="fieldDetails.groundType" element="div" cssClass="error"/>
                </div>
                <div class="col-lg-3">
                    <sf:label path="fieldDetails.grassType">Grass Type</sf:label>
                    <sf:select path="fieldDetails.grassType" items="${grassTypes}" class = "form-control" id = "grassTypeAddField"/>
                    <sf:errors path="fieldDetails.grassType" element="div" cssClass="error"/>
                </div>
                <div class="col-lg-3">
                    <sf:label path="fieldDetails.length">Field Length (meters)</sf:label>
                    <sf:input path="fieldDetails.length" class = "form-control" id = "lengthAddField"/>
                    <sf:errors path="fieldDetails.length" element="div" cssClass="error"/>
                </div>
                <div class="col-lg-3">
                    <sf:label path="fieldDetails.width">Field Width (meters)</sf:label>
                    <sf:input path="fieldDetails.width" class = "form-control" id = "widthAddField"/>
                    <sf:errors path="fieldDetails.width" element="div" cssClass="error"/>
                </div>
            </div>
            <button type="submit" class="btn btn-block btn-primary">Confirm</button>
        </sf:form>
        <c:if test="${addFieldValidationError==null}">
            <script>
                $('#addFieldForm').hide();
            </script>
        </c:if>
    </c:if>

    <h3 class = "text-info">Current Fields</h3>
    <hr>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Field Name</th>
                    <th>Area</th>
                    <th>Field Type</th>
                    <th>Hourly Cost</th>
                    <th>Rating</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${fields}" var="field">
                <tr>
                    <td>
                        <a href="
                            <c:url value="/private/FieldDetailsController">
                                <c:param name="fieldToView" value="${field.fieldId}"/>
                            </c:url>"><c:out value="${field.fieldName}"/>
                        </a>
                    </td>
                    <td><c:out value="${field.address.area}"/></td>
                    <td><c:out value="${field.fieldType}"/></td>
                    <td><c:out value="${field.hourlyCost}"/></td>
                    <td><c:out value="${field.rating}"/></td>
                    <td>
                        <c:if test="${sessionScope.user.userType=='admin'}">
                            <sf:form action="${deleteFieldController}" method="post" modelAttribute="fieldObject">
                                <sf:hidden path="fieldId" value="${field.fieldId}"/>
                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                            </sf:form>
                        </c:if>
                        <c:if test="${sessionScope.user.userType=='public'}">
                            <sf:form action="${rentFieldController}" method="post" modelAttribute="fieldObject">
                                <sf:hidden path="fieldId" value="${field.fieldId}"/>
                                <button type="submit" class="btn btn-sm btn-primary">Rent</button>
                            </sf:form>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <c:if test="${fieldToRent!=null || rentFieldValidationError!=null}">
        <c:import url="/WEB-INF/views/modal/rent_field_modal.jsp"/>
        <script>
            $('#rentFieldModal').modal('show');
        </script>
    </c:if>
    <c:import url="/WEB-INF/views/common/footer.jsp"/>
</div>
</body>
</html>
