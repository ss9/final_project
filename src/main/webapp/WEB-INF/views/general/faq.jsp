<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:import url="/WEB-INF/views/common/head.jsp"/>
    <title>FAQ</title>
    <script src="<c:url value="/js/faq.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/css/general.css"/>" />
    <link rel="stylesheet" href="<c:url value="/css/faq.css"/>" />

</head>
<body>
    <div class="container">
        <c:import url="/WEB-INF/views/common/navbar.jsp"/>

        <div class = "container">
            <h3 class = "text-info">Frequently Asked Questions</h3>
        </div>

        <hr>

        <div class = "container" id = "faqSection">
            <div class = "well faqQuestion">
                <a href = "#">Q. Question?<p class = "faqCaret"><span class = "faqCaret caret"></span></p></a>
            </div>
            <div class = "faqAnswer">
                <p class = "alert alert-info">A. Answer</p>
            </div>
            <div class = "well faqQuestion">
                <a href = "#">Q. Question?<p class = "faqCaret"><span class = "caret"></span></p></a>
            </div>
            <div class = "faqAnswer">
                <p class = "alert alert-info">A. Answer</p>
            </div>
            <div class = "well faqQuestion">
                <a href = "#">Q. Question?<p class = "faqCaret"><span class = "caret"></span></p></a>
            </div>
            <div class = "faqAnswer">
                <p class = "alert alert-info">A. Answer</p>
            </div>
            <div class = "well faqQuestion">
                <a href = "#">Q. Question?<p class = "faqCaret"><span class = "caret"></span></p></a>
            </div>
            <div class = "faqAnswer">
                <p class = "alert alert-info">A. Answer</p>
            </div>
            <div class = "well faqQuestion">
                <a href = "#">Q. Question?<p class = "faqCaret"><span class = "caret"></span></p></a>
            </div>
            <div class = "faqAnswer">
                <p class = "alert alert-info">A. Answer</p>
            </div>
        </div>
        <c:import url="/WEB-INF/views/common/footer.jsp"/>
    </div>
</body>
</html>
