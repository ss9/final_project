<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:import url="/WEB-INF/views/common/head.jsp"/>
    <title>Error Page</title>
</head>
<body>
<div class="container">
    <h1>Hackers not allowed</h1>
    <h1><a href="<c:url value="/LoginController"/>">Index</a></h1>
    <c:import url="/WEB-INF/views/common/footer.jsp"/>
</div>
</body>
</html>
