<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:import url="/WEB-INF/views/common/head.jsp"/>
    <title>About Us</title>
    <script src="<c:url value="/js/index.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/css/general.css"/>" />
    <link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
</head>
<body>

<c:if test="${userObjectLogin==null || userObjectRegistration==null}">
    <c:redirect url="/LoginController"/>
</c:if>

<div class="container">
    <c:import url="/WEB-INF/views/common/navbar.jsp"/>
    <h1>INDEX.JSP</h1>
    USER: <c:out value="${sessionScope.user.fullName}"/>
    <c:import url="/WEB-INF/views/common/footer.jsp"/>
</div>

<c:if test="${registrationValidationError!=null}">
    <script>
        $('#registrationModal').modal('show');
    </script>
</c:if>

<c:if test="${loginValidationError!=null}">
    <script>
        $('#loginModal').modal('show');
    </script>
</c:if>

</body>
</html>
