<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<c:set var="homeController"><c:url value="/private/HomeController"/></c:set>
<c:set var="faqController"><c:url value="/FaqController"/></c:set>
<c:set var="profileController"><c:url value="/private/user/ProfileController"/></c:set>
<c:set var="bookingsController"><c:url value="/private/admin/BookingsController"/></c:set>

<footer>
    <div class = "container text-center">
        <h3>Khelte Chai</h3>
    </div>
    <div class = "container text-center">
        <ul class = "list-inline">
            <c:if test="${sessionScope.user!=null}">
                <li id="fields" class = "list-inline-item"><a href = "${homeController}">Fields</a></li>
                <c:if test="${sessionScope.user.userType=='public'}">
                    <li id="profilePublic" class = "list-inline-item"><a href = "${profileController}">Profile</a></li>
                </c:if>
                <c:if test="${sessionScope.user.userType=='admin'}">
                    <li id="bookingsAdmin" class = "list-inline-item"><a href = "${bookingsController}">Bookings</a></li>
                    <li id="manageUsersAdmin" class = "list-inline-item"><a href = "<c:url value="/private/admin/ManageUsersController"/>">Manage Users</a></li>
                </c:if>
            </c:if>
            <li id="aboutUs" class = "list-inline-item"><a href = "<c:url value="/LoginController"/>">About Us</a></li>
            <li id="faq" class = "list-inline-item"><a href = "${faqController}">FAQ</a></li>
        </ul>
    </div>
</footer>
</body>
</html>
