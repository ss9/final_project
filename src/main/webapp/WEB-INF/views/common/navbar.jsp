<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Navigation</title>
</head>
<body>

<c:set var="homeController"><c:url value="/private/HomeController"/></c:set>
<c:set var="logoutController"><c:url value="/private/LogoutController"/></c:set>
<c:set var="faqController"><c:url value="/FaqController"/></c:set>
<c:set var="profileController"><c:url value="/private/user/ProfileController"/></c:set>
<c:set var="bookingsController"><c:url value="/private/admin/BookingsController"/></c:set>

<c:if test="${sessionScope.user==null}">
    <c:import url="/WEB-INF/views/modal/login_modal.jsp"/>
    <c:import url="/WEB-INF/views/modal/registration_modal.jsp"/>
</c:if>

<nav class = "navbar navbar-default">
    <div class = "navbar-header">
        <a href = "#" class = "navbar-brand">Khelte Chai</a>
        <button class = "navbar-toggle" data-toggle = "collapse" data-target = "#mainNavBar">
            <span class = "icon-bar"></span>
            <span class = "icon-bar"></span>
            <span class = "icon-bar"></span>
        </button>
    </div>

    <div class = "collapse navbar-collapse" id = "mainNavBar">
        <ul class = "nav navbar-nav">
            <c:if test="${sessionScope.user!=null}">
                <li id="fields"><a href = "${homeController}">Fields</a></li>
                <c:if test="${sessionScope.user.userType=='public'}">
                    <li id="profilePublic"><a href = "${profileController}">Profile</a></li>
                </c:if>
                <c:if test="${sessionScope.user.userType=='admin'}">
                    <li id="bookingsAdmin"><a href = "${bookingsController}">Bookings</a></li>
                    <li id="manageUsersAdmin"><a href = "<c:url value="/private/admin/ManageUsersController"/>">Manage Users</a></li>
                </c:if>
            </c:if>
            <li id="aboutUs"><a href = "<c:url value="/LoginController"/>">About Us</a></li>
            <li id="faq"><a href = "${faqController}">FAQ</a></li>
        </ul>

        <ul class = "nav navbar-nav navbar-right">
            <li class = "btn-group">
                <c:if test="${sessionScope.user==null}">
                    <button type = "button" class = "btn btn-primary navbar-btn"
                            data-toggle = "modal" data-target = "#loginModal"
                            id = "loginButton">Login</button>
                    <button type = "button" class = "btn btn-primary navbar-btn"
                            data-toggle = "modal" data-target = "#registrationModal"
                            id = "registrationButton">Registration</button>
                </c:if>
                <c:if test="${sessionScope.user!=null}">
                    <form class="form-inline" action="${logoutController}" method="post" style="margin-bottom: 0">
                        <button type = "submit" class = "btn btn-primary navbar-btn"
                                id = "logoutButton">Logout</button>
                    </form>
                </c:if>
            </li>
        </ul>
    </div>
</nav>
</body>
</html>
