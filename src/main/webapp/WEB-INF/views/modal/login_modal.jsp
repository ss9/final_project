<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Modal</title>
</head>
<body>

<c:set var="loginController"><c:url value="/LoginController"/></c:set>

<div class = "modal fade" id = "loginModal">
    <div class = "modal-dialog modal-sm">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type = "button" class = "close" data-dismiss = "modal">&times;</button>
                <h3 class = "modal-title text-info">Login</h3>
            </div>
            <div class = "modal-body">
                <sf:form id = "loginForm" modelAttribute="userObjectLogin"
                         action="${loginController}" method="post">
                    <sf:hidden path="fullName" value="Full Name"/>
                    <div class = "form-group">
                        <sf:label path="username">Username:</sf:label>
                        <sf:errors path="username" element="div" cssClass="error"/>
                        <sf:input path="username" class = "form-control" id = "usernameLogin"/>
                    </div>
                    <div class = "form-group">
                        <sf:label path="password">Password</sf:label>
                        <sf:errors path="password" element="div" cssClass="error"/>
                        <sf:input path="password" class = "form-control" id = "passwordLogin"/>
                    </div>
                    <sf:hidden path="email" value="abcde@example.com"/>
                    <sf:hidden path="phone" value="1234567890"/>
                </sf:form>
            </div>
            <div class = "modal-footer">
                <input type = "submit" form = "loginForm" class = "btn btn-success" id = "loginSubmit"
                       value = "Login">
                <button type = "button" class = "btn btn-primary" data-dismiss = "modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
