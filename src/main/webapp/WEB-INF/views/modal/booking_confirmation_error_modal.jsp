
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Booking Confirmation Error Modal</title>
</head>
<body>
<div class = "modal fade" id = "bookingConfirmationErrorModal">
    <div class = "modal-dialog modal-sm">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type = "button" class = "close" data-dismiss = "modal">&times;</button>
                <h3 class = "modal-title text-info">Login</h3>
            </div>
            <div class = "modal-body">
                This field is not available on this day and time
            </div>
            <div class = "modal-footer">
                <button type = "button" class = "btn btn-primary" data-dismiss = "modal">Sorry</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
