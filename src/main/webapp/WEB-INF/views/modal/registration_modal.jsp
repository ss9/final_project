<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration Modal</title>
</head>
<body>

<c:set var="registrationController"><c:url value="/RegistrationController"/></c:set>

<div class = "modal fade" id = "registrationModal">
    <div class = "modal-dialog modal-md">
        <div class = "modal-content">
            <div class = "modal-header">
                <button type = "button" class = "close" data-dismiss = "modal">&times;</button>
                <h3 class = "modal-title text-info">Registration</h3>
            </div>
            <div class = "modal-body">
                <sf:form id = "registrationForm" modelAttribute="userObjectRegistration"
                        action="${registrationController}" method="post">
                    <div class = "form-group">
                        <sf:label path="fullName">Full Name</sf:label>
                        <sf:errors path="fullName" element="div" cssClass="error"/>
                        <sf:input path="fullName" class = "form-control" id = "fullNameRegistration"/>
                    </div>

                    <div class = "form-group">
                        <sf:label path="username">Username</sf:label>
                        <sf:errors path="username" element="div" cssClass="error"/>
                        <sf:input path="username" class = "form-control" id = "usernameRegistration"/>
                    </div>

                    <div class = "form-group">
                        <sf:label path="password">Password</sf:label>
                        <sf:errors path="password" element="div" cssClass="error"/>
                        <sf:input path="password" class = "form-control" id = "passwordRegistration"/>
                    </div>

                    <div class = "form-group">
                        <sf:label path="email">Email</sf:label>
                        <sf:errors path="email" element="div" cssClass="error"/>
                        <sf:input path="email" class = "form-control" id = "emailRegistration"/>
                    </div>

                    <div class = "form-group">
                        <sf:label path="phone">Phone</sf:label>
                        <sf:errors path="phone" element="div" cssClass="error"/>
                        <sf:input path="phone" class = "form-control" id = "phoneRegistration"/>
                    </div>
                </sf:form>
            </div>
            <div class = "modal-footer">
                <input type = "submit" form = "registrationForm" class = "btn btn-success" id = "registrationSubmit"
                        value = "Register">
                <button type = "button" class = "btn btn-primary" data-dismiss = "modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
