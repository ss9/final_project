<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Rent Field Modal</title>
</head>
<body>
<c:set var="rentFieldController"><c:url value="/private/user/RentFieldController"/> </c:set>
<div id="rentFieldModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Rent Field</h4>
            </div>
            <div class="modal-body">
                <sf:form  id="rentFieldForm" modelAttribute="bookingObject"
                          action="${rentFieldController}" method="post">
                    <sf:hidden path="userId" value="${sessionScope.user.userId}"/>
                    <sf:hidden path="fieldId" value="${fieldToRent.fieldId}"/>
                    <div class="form-group">
                        <sf:label path="bookingDay">Day</sf:label>
                        <sf:errors path="bookingDay" element="div" cssClass="error"/>
                        <sf:input type="date" path="bookingDay" class = "form-control" id = "bookingDayRentField"/>
                    </div>
                    <div class="form-group">
                        <sf:label path="startTime">Start Time</sf:label>
                        <sf:errors path="startTime" element="div" cssClass="error"/>
                        <sf:input type="time" step="1" path="startTime"
                                  class="form-control" id="startTimeRentField"/>
                    </div>
                    <div class="form-group">
                        <sf:label path="endTime">End Time</sf:label>
                        <sf:errors path="endTime" element="div" cssClass="error"/>
                        <sf:input type="time" step="1" path="endTime"
                                  class="form-control" id="endTimeRentField"/>
                    </div>
                    Cost: <c:out value="${fieldToRent.hourlyCost}"/>
                </sf:form>
            </div>
            <div class="modal-footer">
                <input type = "submit" form = "rentFieldForm" class = "btn btn-success" id = "rentFieldSubmit"
                       value = "Request to Rent">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
