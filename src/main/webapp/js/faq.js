$(window).on('load', start);

function start() {
    $('#faq').addClass('active');

    hideShowAnswers();
}

function hideShowAnswers(){
    $('.faqAnswer').hide();
    $('.faqQuestion').click(function(){
        $(this).next().slideToggle('slow');
    });
}
